function x = IABA
global L;
global Wmax;
global N;
global m;
global maxBE;
global minBE;
SimTime=1000000*(0.32/1000000);

L = 14;
m = 7;
n = 3;
maxBE = 11;
minBE = 3;
Wmax = 2^maxBE;
nCCA = 2;
i = 0;
Pbo  = 0.8;       % average power consumption (in mW) during "idle-listen" state (it is referred to as Pi in Park's paper)
Pcca = 40;        % average power consumption (in mW) during "channel sensing" state (it is referred to as Psc in Park's paper)
Pt  = 30;        % average power consumption (in mW) during "transmit" state
Pr  = 40;        % average power consumption (in mW) during "receive" state

for N = [5 10 15 20 25 30 35 40 45 50 55 60 65 70 80 90 100 120 140 160 180 200 240 290 340 400]

i = i + 1;

NN(i) = N;

        x0 = [0.001; 0.001; 0.001]; % x0 = [a1 ; a2 ; t]
        x = fsolve(@IABA_h_2_fun,x0);

        a1 = x(1);
        a2 = x(2);
        t = x(3);
k = 0;
while(a1 < 0 || a1 > 1 || a2 < 0 || a2 > 1 || t < 0 || t > 1)     
        k = k + 0.01;    
        x0 = [0.001+k; 0.001+k; 0.001+k; ]; % x0 = [a1 ; a2 ; a3 ; a4; a5; t]
        x = fsolve(@IABA_h_2_fun,x0);

        a1 = x(1);
        a2 = x(2);
        t = x(3);
end

b11 = t;
Pc = 1-(1-t)^(N-1);
Pcol(i) = Pc;
tau(i)=t;
%-----Channel Utilization-----------------------------------

U(i)=NN(i)*L*t*((1-t)^(NN(i)-1))*(1-a1)*(1-a2);


%-----------------------------------------------------------
%Idle time

I(i) = 1-a1;

%-----Collision Time ---------------------------------------

C(i)= 1-U(i)-I(i);

%-----Delay-------------------------------------------------

T_CCA(i) = a1+2*(1-a1);


PI_BC=0;
for ii=1:nCCA
    pp1=1;
    for jj=1:ii-1
        pp1=pp1*(1-x(jj));
    end
    PI_BC=PI_BC+x(ii)*pp1;
end


%T_BO(i)=(2^(minBE-1))*(1-PI_BC+PI_BC^(m+1)*((1-PI_BC)/(1-PI_BC^(m+1)))+2*PI_BC*((1-PI_BC)/(1-PI_BC^(m+1)))*((1-(2*PI_BC)^m)/(1-2*PI_BC)));
T_BO(i)=(0.8*(0.004/(0.001+t*(1+1-a1+L*(1-a1)*(1-a2)))-0.003)+0.2*Pc)*Wmax/2;
%T_BO(i)=Pc*Wmax;
PI_CC=1;
for ii=1:nCCA
    PI_CC=PI_CC*(1-x(ii));
end
PI_CC=PI_CC*Pc

PI_SC=1;
for ii=1:2
    PI_SC=PI_SC*(1-x(ii));
end
PI_SC=PI_SC*(1-Pc);

coeff=(1+PI_CC/PI_SC+PI_BC/PI_SC);
D(i)=(1+PI_CC/PI_SC+PI_BC/PI_SC)*T_BO(i)+(PI_BC/PI_SC)*T_CCA(i)+(1+PI_CC/PI_SC)*(2+L);

    
%-----------------------------------------------------------

%-----Power measurements-----------------------------------

%the second term in the following equation refers to the power consumed during the backoff that occurs if CCA3 is busy
Ebo(i) = Pbo*(((0.8*(0.004/(0.001+t*(2-a1+L*(1-a1)*(1-a2)))-0.003)+0.2*Pc)*Wmax-1)/2)*b11; 
 
Ecca(i) = Pcca*(2-a1)*b11;
 
Et(i) = Pt*L*(1-a1)*(1-a2)*b11;
 
Er(i) = Pr*L*(1-a1)*(1-a2)*(1-Pc)*b11;
 
Etot(i) = (SimTime)*(Ebo(i) + Ecca(i) + Et(i) + Er(i));

Ec(i) = (SimTime)*(Pt*L*(1-a1)*(1-a2)*b11*Pc);

%-----Reliability-----------------------------------
x = PI_BC;
y = PI_CC;
R(i) = 1/(1+((1-x)*x^(m+1)/((1-x^(m+1))*(1-x-y))) + (y^(n+1))/((1-x)^(n+1)-y^(n+1)));
%---------------------------------------------------
Pcol(i);
end

clc;
fprintf("Probability Of Collision");
Pcol'
fprintf("Throughput");
U'
fprintf("Idle");
I'
fprintf("Collision");
C'
fprintf("Delay");
D'
fprintf("Reliability");
R'
fprintf("Energy");
Etot'
fprintf("Energy Of Collision");
Ec'

